import datetime
import sqlite3
import time
from datetime import datetime
now = time.time()
def Check_List(classListing):
    flightlst = []
    for i in classListing:
        flightlst.append(i.cs)
    return flightlst


def Check_Location(classListing, data):
    last_update = False
    for n, i in enumerate(classListing):
        if i.cs == data['flight'] and i.index_location == data['index_loc']:
            last_update = n
    return last_update


def reduceImpact(x, y, z, skipCount=3):
    finalX, finalY, finalZ = [], [], []
    for test in range(len(x)):
        if test % skipCount == 0:
            finalX.append(x[test])
            finalY.append(y[test])
            finalZ.append(z[test])
    return finalX, finalY, finalZ


def secondsince(dateTime):
    start = datetime.datetime.strptime(dateTime, '%Y-%m-%d %H:%M:%S')
    return time.mktime(start.timetuple())


class flightList():
    def __init__(self, data, local=0):
        self.cs = str(data['flight'])
        self.lat = str(data['lat'])
        self.lon = str(data['lon'])
        self.alt = str(data['alt'])
        self.vert = str(data['vr'])
        self.heading = str(data['heading'])
        self.speed = str(data['speed'])
        self.time_update = [datetime.fromtimestamp(data['last_update'])]
        self.last_seen = self.time_update[-1]
        self.location_history_lat = []
        self.location_history_lon = []
        self.location_history_altitude = []
        self.index_location = local
        self.count = 0

    def update(self, data):
        self.count += 1
        self.location_history_lat.append(data['lat'])
        self.location_history_lon.append(data['lon'])
        self.location_history_altitude.append(data['alt'])
        self.time_update.append(datetime.fromtimestamp(data['last_update']))
        self.last_seen = self.time_update[-1]


conn = sqlite3.connect("basestation.New.sqb")

conn.row_factory = sqlite3.Row

c = conn.cursor()

rowList = []
callsignList = []
for row in c.execute("SELECT * FROM flightslog WHERE flight != ''"):

    if row['flight'] not in Check_List(rowList):
        rowList.append(flightList(row, row['index_loc']))
    else:
        rowList[Check_Location(rowList, row)].update(row)

conn.close()


occurrence = []
for i in rowList:
    occurrence.append((i.cs, i.index_location, i.count))

occurrence.sort(key=lambda tup: tup[2], reverse=True)

for i in occurrence[:10]:
	print(i[0],i[2])
print(f"The program completed in {round(time.time()-now,2)} seconds")

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')




getLst = []
for i, l, n in occurrence:
    getLst.append((i, l))
print("Final step")

for i, l in getLst:
    for o in rowList:
        if o.cs == i and o.index_location == l:
            x, y, z = reduceImpact(o.location_history_lat, o.location_history_lon, o.location_history_altitude, 10)

            plt.scatter(x, y, s=0.1)

#

# ax.set_xlabel('Lat')
# ax.set_ylabel('Lon')
#ax.set_zlabel('Alt')

plt.show()

