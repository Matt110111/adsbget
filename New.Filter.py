import sqlite3
from datetime import datetime
Program_Start = datetime.now()
#conn = sqlite3.connect("basestation.sqb")
conn= sqlite3.connect("./tools/database/basestation.New.sqb")
conn.row_factory = sqlite3.Row
#conn1.row_factory= sqlite3.Row
c = conn.cursor()
#b = conn1.cursor()
#Settings
Drop_Threashold = 100


Nplanes = {}
Nplanes_Final = {}
latest = 0
Total_DB_Lines = 0
##Return a list of call signs to remove from main list.




#Setup
##Initial Filter to remove blacks and outliers
for row in c.execute("SELECT * FROM flightslog"):
        if row['flight'] != '' and row['lat'] != 0 and row['lat'] != None and row['lon'] != 0 and row['lon'] != None and row['heading'] != 0 and row['alt'] != 0 and row['last_update'] != None:
            
            Nplanes[latest] = dict(flight=row['flight'], altitude=row['alt'], vert_rate=row['vr'], latitude=float(row['lat']),
                               longitude=float(row['lon']), heading=row['heading'], speed=row['speed'],
                               last_update=row['last_update'])
            latest += 1
        Total_DB_Lines += 1
#Initial filter
Flight_Drop_LIST,start_LIST = [],[]
for row in c.execute("SELECT flight FROM flightslog"):
    start_LIST.append(row[0])
print("Done Creating List")
print(len(set(start_LIST)))
for item in set(start_LIST):
    if start_LIST.count(item) < Drop_Threashold:
        Flight_Drop_LIST.append(item)


print(len(set(start_LIST))-len(Flight_Drop_LIST))
c.close()

print("DB closed\nLines before initial filter: %s\nLines before DEDUP: %s") % (Total_DB_Lines,len(Nplanes))
#Filtering Duplicates

Failure = False
Iter = 0
End_plate = -1
sample_Size = -1
failure_Rate = 0



for iteration in range(len(Nplanes)-1,End_plate,-1):
    row = Nplanes[iteration]
    
    Row_test = (row['flight'],row['latitude'],row['longitude'],row['altitude'])
    
    if iteration > 500:
        sample_Size = iteration-500
    else:
        sample_Size = -1
    
    
    for two_Row_test in range(iteration-1,sample_Size,-1):
        twodeep = Nplanes[two_Row_test]
        if Row_test == (twodeep['flight'],twodeep['latitude'],twodeep['longitude'],twodeep['altitude']):
            Failure = True

    if Failure == False: 
        Nplanes_Final[Iter] = row
        Iter += 1
    else:
        failure_Rate += 1
    Failure = False


print("Deduplication completed.\nLines after DEDUP: %s") % (failure_Rate)
#print "Total Networks created: %s" % (max(flightNum))
print("Total lines: %s\nDumping filtered output to file." )% len(Nplanes_Final)
CallSigns = []
f = open("Nplanes.db", "wb")
total = 0


for row in Nplanes_Final:
    A_row = Nplanes_Final[row]
    #if A_row['flight'] not in Flight_Drop_LIST:
    #if float(A_row['longitude']) > 40.683242 and float(A_row['longitude']) < 43.279705 and float(A_row['latitude']) > (-73.364365) and float(A_row['latitude']) < (-69.833290):
            #b.execute("INSERT INTO flightslog(flight,alt,vr,heading,speed,lat,lon,last_update) VALUES(?,?,?,?,?,?,?,?)", (
            #    A_row['flight'], A_row['altitude'], A_row['vert_rate'],
            #    A_row['heading'], A_row['speed'],
            #    A_row['latitude'], A_row['longitude'],A_row['last_update']))
    data = str([str(A_row['flight']),A_row['altitude'],A_row['vert_rate'],A_row['longitude'],A_row['latitude'],A_row['speed'],A_row['heading'],str(A_row['last_update'])])[1:-1]+"\n"
    f.write(data)
    total += 1
print(total)  
#conn1.commit()
f.close()






print("Program Completed Successful\nProgram took %s seconds to completed.") % ((datetime.now()-Program_Start).seconds)
