import sqlite3,os

os.chdir('/Users/matthewjohnson/PycharmProjects/adsbget/tools')
conn = sqlite3.connect('./basestation.New.sqb')
c = conn.cursor()

print "Reading in 'Nplanes.db'."
f = open("../Nplanes.db","rb")
Nplanes = {}
Increament = 0
for line in f:
    Data = line[:-1].split(', ')
    Data[0],Data[-1] = Data[0][1:-1],Data[7][1:-1]
    #print Data
    Nplanes[Increament] = {'flight':Data[0],'altitude':Data[1],'vert_rate':Data[2],'latitude':Data[3],'longitude':Data[4],'speed':Data[5],'heading':Data[6],'last_update':Data[7]}
    Increament += 1
print "Commiting to Database."
Iteration_Var = 0
Iteration_Var_Facade = 0
for i in Nplanes:
        if Iteration_Var == 1000:
            Iteration_Var_Facade += 1000
            print(Iteration_Var_Facade)
            Iteration_Var = 0
        # print Nplanes[i]
        c.execute("INSERT INTO flightslog(flight,alt,vr,heading,speed,lat,lon) VALUES(?,?,?,?,?,?,?)", (
            Nplanes[i]['flight'], Nplanes[i]['altitude'], Nplanes[i]['vert_rate'],
            Nplanes[i]['heading'], Nplanes[i]['speed'],
            Nplanes[i]['latitude'], Nplanes[i]['longitude']))
        conn.commit()         

        Iteration_Var += 1