import datetime
import sqlite3
import time

Time_begin = time.time()

def Check_List(classListing):
    flightlst = []
    for i in classListing:
        flightlst.append(i.cs)
    return flightlst


def Check_Location(classListing, data):
    for n, i in enumerate(classListing):
        if i.cs == data['flight']:
            last_update = n
    return last_update


def Critera_met(row):
    if ' ' not in row['flight'][:-2] and '?' not in row['flight'] and 41 \
            <= row['lat'] <= 42.5 and -73.25 <= row['lon'] \
            <= -70 and row['alt'] >= 150 and row['flight'] != 'TEST1234':
        return True
    else:
        return False


def reduceImpact(x, y, z, skipCount=3):
    finalX, finalY, finalZ = [], [], []
    for test in range(len(x)):
        if test % skipCount == 0:
            finalX.append(x[test])
            finalY.append(y[test])
            finalZ.append(z[test])
    return finalX, finalY, finalZ


def secondsince(dateTime):
    start = datetime.datetime.strptime(dateTime, '%Y-%m-%d %H:%M:%S')
    return time.mktime(start.timetuple())


class flightList():
    def __init__(self, data, local=0):
        self.cs = str(data['flight'])
        self.lat = str(data['lat'])
        self.lon = str(data['lon'])
        self.alt = str(data['alt'])
        self.vert = str(data['vr'])
        self.heading = str(data['heading'])
        self.speed = str(data['speed'])
        self.time_update = [time.mktime(
            datetime.datetime.strptime(data['last_update'][0:19], '%Y-%m-%d %H:%M:%S').timetuple())]
        self.last_seen = self.time_update[-1]
        self.location_history_lat = []
        self.location_history_lon = []
        self.location_history_altitude = []
        self.index_location = local

    def update(self, datas):
        # print len(self.location_history_lat)
        self.location_history_lat.append(datas['lat'])
        self.location_history_lon.append(datas['lon'])
        self.location_history_altitude.append(datas['alt'])
        self.time_update.append(time.mktime(
            datetime.datetime.strptime(datas['last_update'][0:19], '%Y-%m-%d %H:%M:%S').timetuple()))
        self.last_seen = self.time_update[-1]


conn = sqlite3.connect("./database/basestation.sqb")
conn1 = sqlite3.connect("./database/basestation.New.sqb")
conn.row_factory = sqlite3.Row
conn1.row_factory = sqlite3.Row
c = conn.cursor()
b = conn1.cursor()

rowList = []

for row in c.execute("SELECT * FROM flightslog WHERE flight != ''"):
    if Critera_met(row):

        if row['flight'] not in Check_List(rowList):

            rowList.append(flightList(row))
        else:
            test_statement = Check_Location(rowList, row)
            update_time = secondsince(row['last_update'][:19])

            if row['lat'] not in rowList[test_statement].location_history_lat and row['lon'] not in rowList[
                test_statement].location_history_lon:
                notAduplicate = True
            else:
                notAduplicate = False

            if update_time - rowList[test_statement].last_seen <= 500 and notAduplicate:
                # Test statement for length of intermission between signals
                rowList[test_statement].update(row)
            if update_time - rowList[test_statement].last_seen > 500:
                rowList.append(flightList(row, rowList[test_statement].index_location + 1))

print "Dump completed."

callsignList = []
for row in rowList:
    if row.cs not in callsignList:
        callsignList.append(row.cs)

print "Created list of callsigns."

for flight in callsignList:
    for row in rowList:
        if flight == row.cs:
            for data in range(len(row.location_history_lat)):
                b.execute(
                    "INSERT INTO flightslog(flight,alt,speed,lat,lon,last_update,index_loc) VALUES(?,?,?,?,?,?,?)", (
                        row.cs, row.alt, row.speed, row.location_history_lat[data],
                        row.location_history_lon[data], row.time_update[data], row.index_location))
                conn1.commit()
print "Completed upload to new database."

print "Total time: %f" % float(time.time()-Time_begin)

conn.close()
conn1.close()

print  "Done."
