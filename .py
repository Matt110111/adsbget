import sqlite3
import re

conn = sqlite3.connect('basestation.sqb')
conn.row_factory = sqlite3.Row
c = conn.cursor()
altlst = []
latlst = []
lonlst = []
ncall = []
nalt = []
nlat = []
nlon = []
flightlst = []
lastcall = 0
a = 0
BadSTR = var = 0
start = input("Enter the lowest occurence\nthen press enter: ")

blacklist = ['TEPTSZ34', 'TMWT1224', 'TUST123T', 'TEQT02Q6', 'TESTP214', '', ' ', '  ', '   ', '    ', '     ',
             '       ', '       ', '        ', 'TEST9234', 'TEST0234', 'TESD1234', 'TEQT9214', 'TEST0214', 'TEST123T',
             'TEQT1214', 'TEQTP234', 'TEST12VT', 'LEST1234', 'TEQVY2Z4', 'TEST3234', 'TEST1234', 'TEST1214', 'TEST',
             '1234', '?234', 'TESTQ234', 'TEST327?', 'TEST1236', 'TEQT0214', 'TEWT123?', 'TEST1?3?', 'TEST12S4',
             'TEQTP2Q4', 'TEST1R34', 'TEST12Q4', 'TMST1234', 'TEST02Q4', 'TES?123?', 'Y???D???', 'TEST12?4', 'TEST123?',
             'TAST1234']
for row in c.execute("SELECT * FROM flightslog WHERE flight != ''"):
    if re.search('[?]', str(row['flight'])):
        a = 1
    if 'TES' in row['flight']:
        a = 1
    if 'EST' in row['flight']:
        a = 1
    if '1234' in row['flight']:
        a = 1
    for word in blacklist:
        if str(row['flight']) in blacklist:
            BadSTR += 1
            a = a + 1
    if a == 0:
        if row['lat'] >= -40:
            if row['lat'] >= 41:
                if row['lon'] >= -140 and row['lon'] <= -50 and row['alt'] >= 0:
                    flightlst.append(row['flight'])
                    altlst.append(row['alt'])
                    latlst.append(row['lat'])
                    lonlst.append(row['lon'])

    a = 0
c = conn.close()
callsign = []
for item in flightlst:
    if item not in callsign:
        callsign.append(item)

testit = 0
FinalFlst = []
for item1 in callsign:
    if flightlst.count(item1) > int(start):
        FinalFlst.append(item1)
        testit = testit + 1
        if testit > 3:
            testit = 0
        else:
            # print item1, flightlst.count(item1),
            pass

for a in FinalFlst:
    for aitem in range(0, len(flightlst)):
        if a == flightlst[aitem]:
            ncall.append(a)
            nalt.append(altlst[aitem])
            nlat.append(latlst[aitem])
            nlon.append(lonlst[aitem])
# print ncall
#print nalt
#print nlat
#print nlon

end = str(input("End Process"))

if end == '1':
    print(len(nalt))
    import matplotlib.pyplot as plt

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(41.731215, -71.407801, 50, c='r')
    ax.scatter(nlat, nlon, nalt)
    ax.set_xlabel('Lat')
    ax.set_ylabel('Lon')
    ax.set_zlabel('Alt')
    plt.show()
